package myphamseoul.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import myphamseoul.base.MapperBase;
import myphamseoul.dto.ColorDto;
import myphamseoul.entity.ColorEntity;

@Component
public class ColorMapper implements MapperBase<ColorEntity, ColorDto>{

	@Override
	public ColorEntity toEntity(ColorDto dto) {
		return mapper.map(dto, ColorEntity.class);
	}

	@Override
	public ColorDto toDto(ColorEntity entity) {
		return mapper.map(entity, ColorDto.class);
	}

	@Override
	public List<ColorEntity> toEntity(List<ColorDto> dtos) {
		List<ColorEntity> entitys = new ArrayList<>();
		for(ColorDto dto : dtos) {
			entitys.add(toEntity(dto));
		}
		return entitys;
	}

	@Override
	public List<ColorDto> toDto(List<ColorEntity> entitys) {
		List<ColorDto> dtos = new ArrayList<>();
		for(ColorEntity entity : entitys) {
			dtos.add(toDto(entity));
		}
		return dtos;
	}

}
