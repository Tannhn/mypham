package myphamseoul.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import myphamseoul.base.MapperBase;
import myphamseoul.dto.BrandLineProductDto;
import myphamseoul.entity.BrandEntity;
import myphamseoul.entity.BrandLineProductEntity;
import myphamseoul.entity.BrandLineProductKey;
import myphamseoul.entity.LineProductEntity;
import myphamseoul.service.BrandService;
import myphamseoul.service.LineProductService;

@Component
public class BrandLineProductMapper implements MapperBase<BrandLineProductEntity, BrandLineProductDto> {
	
	@Autowired
	private BrandService brandService;
	
	@Autowired
	private LineProductService lineProductService;

	@Override
	public BrandLineProductEntity toEntity(BrandLineProductDto dto) {
		if(dto!=null) {
			LineProductEntity lineproductE = lineProductService.findOneByName(dto.getLineProductName());
			if(dto.getBrandId()==null) {
				BrandEntity brandE = brandService.findOneByName(dto.getBrandName());
				if(brandE!=null&&lineproductE!=null) {
					return new BrandLineProductEntity(new BrandLineProductKey(brandE.getId(),lineproductE.getId()),brandE,lineproductE);
				}
			}else {
				BrandEntity brandE = brandService.getById(dto.getBrandId());
				if(brandE!=null&&lineproductE!=null) {
					return new BrandLineProductEntity(new BrandLineProductKey(brandE.getId(),lineproductE.getId()),brandE,lineproductE);
				}
			}
		}
		return null;
	}

	@Override
	public BrandLineProductDto toDto(BrandLineProductEntity entity) {
		if(entity!=null) {
			return mapper.map(entity, BrandLineProductDto.class);
		}
		return null;
	}

	@Override
	public List<BrandLineProductEntity> toEntity(List<BrandLineProductDto> dtos) {
		List<BrandLineProductEntity> entitys = new ArrayList<>();
		for(BrandLineProductDto dto : dtos) {
			entitys.add(toEntity(dto));
		}
		return entitys;
	}

	@Override
	public List<BrandLineProductDto> toDto(List<BrandLineProductEntity> entitys) {
		List<BrandLineProductDto> dtos = new ArrayList<>();
		for(BrandLineProductEntity entity : entitys) {
			dtos.add(toDto(entity));
		}
		return dtos;
	}

}
