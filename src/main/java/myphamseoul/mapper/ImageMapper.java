package myphamseoul.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import myphamseoul.base.MapperBase;
import myphamseoul.dto.ImageDto;
import myphamseoul.entity.ImageEntity;

@Component
public class ImageMapper implements MapperBase<ImageEntity, ImageDto> {

	@Override
	public ImageEntity toEntity(ImageDto dto) {
		if(dto!=null) {
			return mapper.map(dto, ImageEntity.class);
		}
		return null;
	}

	@Override
	public ImageDto toDto(ImageEntity entity) {
		if(entity!=null) {
			return mapper.map(entity, ImageDto.class);
		}
		return null;
	}

	@Override
	public List<ImageEntity> toEntity(List<ImageDto> dtos) {
		return dtos.stream().map(dto -> {return toEntity(dto);}).collect(Collectors.toList());
	}

	@Override
	public List<ImageDto> toDto(List<ImageEntity> entitys) {
		return entitys.stream().map(entity -> {return toDto(entity);}).collect(Collectors.toList());
	}

}
