package myphamseoul.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import myphamseoul.base.MapperBase;
import myphamseoul.dto.ImageColorDto;
import myphamseoul.dto.ProductMainDto;
import myphamseoul.entity.BrandLineProductEntity;
import myphamseoul.entity.CategoryEntity;
import myphamseoul.entity.ColorEntity;
import myphamseoul.entity.ImageEntity;
import myphamseoul.entity.LineProductEntity;
import myphamseoul.entity.ProductEntity;
import myphamseoul.entity.ProductMainEntity;
import myphamseoul.entity.PromotionEntity;
import myphamseoul.entity.TagEntity;
import myphamseoul.service.BrandLineProductService;
import myphamseoul.service.CategoryService;
import myphamseoul.service.ImageService;
import myphamseoul.service.LineProductService;
import myphamseoul.service.ProductService;
import myphamseoul.service.ProductTagService;
import myphamseoul.service.PromotionService;

@Component
public class ProductMainMapper implements MapperBase<ProductMainEntity, ProductMainDto> {
	
	@Autowired
	private BrandLineProductService brandLineProductService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private LineProductService lineProductService;
	
	@Autowired
	private ProductTagService productTagService;
	
	@Autowired
	private ImageService imageService;
	
	@Autowired
	private ColorMapper colorMapper;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private PromotionService promotionService;

	@Override
	public ProductMainEntity toEntity(ProductMainDto dto) {
		if(dto!=null) {
			ProductMainEntity entity = mapper.map(dto, ProductMainEntity.class);
			CategoryEntity categoryE = categoryService.findOneByName(dto.getCategoryName());
			if(categoryE!=null) {
				entity.setCategory(categoryE);
			}
			LineProductEntity lineProductE = lineProductService.findOneByName(dto.getLineProductName());
			if(lineProductE!=null&&dto.getBrandId()!=null) {
				BrandLineProductEntity blp = brandLineProductService.findByBrandIdAndLineProductId(dto.getBrandId(),lineProductE.getId());
				entity.setBrandLineProduct(blp);
			}
			return entity;
			
		}
		return null;
	}

	@Override
	public ProductMainDto toDto(ProductMainEntity entity) {
		if(entity!=null) {
			ProductMainDto dto = mapper.map(entity, ProductMainDto.class);
			List<TagEntity> tagEs = productTagService.findProductMainByProductMainId(dto.getId());
			if(tagEs.size()>0) {
				List<String> tagNames = new ArrayList<>();
				for(TagEntity tag : tagEs) {
					tagNames.add(tag.getName());
				}
				dto.setTags(tagNames);
			}
			if(entity.getId()!=null) {
				List<PromotionEntity> promEs = promotionService.findPromotionByProductProductMainId(entity.getId());
				if(promEs.size()>0) {
					PromotionEntity promE = promEs.get(0);
					if(promE!=null) {
						dto.setSale(promEs.get(0).getSale());
						List<ProductEntity> pros = productService.findProductByPromotionIdMax(promE.getId());
						if(pros!=null&&pros.size()>0) {
							dto.setPrice(pros.get(0).getPrice());
						}
					}
					
				}
				List<ImageEntity> imgEs = imageService.findByProductMainId(entity.getId());
				if(imgEs.size()>0) {
					Set<ColorEntity> colorEs = new HashSet<>();
					for(ImageEntity imgE : imgEs) {
						colorEs.add(imgE.getProduct().getColor());
					}
					Set<ImageColorDto> imageColors = new HashSet<>();
					colorEs.forEach(colorE ->{
						ImageColorDto imageColor = new ImageColorDto();
						imageColor.setColor(colorMapper.toDto(colorE));
						Set<String> imageIds = new HashSet<>();
						for(ImageEntity imgE : imgEs) {
							if(imgE.isMain()) {
								dto.setImageMainId(imgE.getId());
							}
							if(imgE.isHover()) {
								dto.setImageHoverId(imgE.getId());
							}
							if(colorE.equals(imgE.getProduct().getColor())) {
								imageIds.add(imgE.getId());
							}
						}
						imageColor.setImageIds(imageIds);
						imageColors.add(imageColor);
					});
					dto.setImageColors(imageColors);
				}
			}
			
			return dto;
		}
		return null;
	}

	@Override
	public List<ProductMainEntity> toEntity(List<ProductMainDto> dtos) {
		List<ProductMainEntity> entitys = new ArrayList<>();
		for(ProductMainDto dto : dtos) {
			entitys.add(toEntity(dto));
		}
		return entitys;
	}

	@Override
	public List<ProductMainDto> toDto(List<ProductMainEntity> entitys) {
		List<ProductMainDto> dtos = new ArrayList<>();
		for(ProductMainEntity entity : entitys) {
			dtos.add(toDto(entity));
		}
		return dtos;
	}

}
