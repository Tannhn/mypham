package myphamseoul.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import myphamseoul.base.MapperBase;
import myphamseoul.dto.LineProductDto;
import myphamseoul.entity.LineProductEntity;
import myphamseoul.service.BrandLineProductService;

@Component
public class LineProductMapper implements MapperBase<LineProductEntity, LineProductDto>{
	
	@Autowired
	private BrandLineProductService brandLineProductService;

	@Override
	public LineProductEntity toEntity(LineProductDto dto) {
		if(dto!=null) {
			return mapper.map(dto, LineProductEntity.class);
		}
		return null;
	}

	@Override
	public LineProductDto toDto(LineProductEntity entity) {
		if(entity!=null) {
			LineProductDto dto = mapper.map(entity, LineProductDto.class);
			if(entity.getId()!=null) {
				List<String> brands = new ArrayList<>();
				brandLineProductService.findBrandByLineProductId(entity.getId()).forEach(brand -> {
					brands.add(brand.getName());
				});
				dto.setBrands(brands);
			}
			return dto;
		}
		return null;
	}

	@Override
	public List<LineProductEntity> toEntity(List<LineProductDto> dtos) {
		List<LineProductEntity> entitys = new ArrayList<>();
		for(LineProductDto dto : dtos) {
			entitys.add(toEntity(dto));
		}
		return entitys;
	}

	@Override
	public List<LineProductDto> toDto(List<LineProductEntity> entitys) {
		List<LineProductDto> dtos = new ArrayList<>();
		for(LineProductEntity entity : entitys) {
			dtos.add(toDto(entity));
		}
		return dtos;
	}

}
