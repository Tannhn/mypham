package myphamseoul.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import myphamseoul.base.MapperBase;
import myphamseoul.dto.TagDto;
import myphamseoul.entity.TagEntity;

@Component
public class TagMapper implements MapperBase<TagEntity, TagDto>{

	@Override
	public TagEntity toEntity(TagDto dto) {
		if(dto!=null) {
			return mapper.map(dto, TagEntity.class);
		}
		return null;
	}

	@Override
	public TagDto toDto(TagEntity entity) {
		if(entity!=null) {
			return mapper.map(entity, TagDto.class);
		}
		return null;
	}

	@Override
	public List<TagEntity> toEntity(List<TagDto> dtos) {
		return dtos.stream().map(dto -> {return toEntity(dto);}).collect(Collectors.toList());
	}

	@Override
	public List<TagDto> toDto(List<TagEntity> entitys) {
		return entitys.stream().map(entity -> {return toDto(entity);}).collect(Collectors.toList());
	}

}
