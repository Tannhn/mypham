package myphamseoul.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import myphamseoul.base.MapperBase;
import myphamseoul.dto.BrandDto;
import myphamseoul.entity.BrandEntity;
import myphamseoul.service.BrandLineProductService;

@Component
public class BrandMapper implements MapperBase<BrandEntity, BrandDto>{
	
	@Autowired
	private BrandLineProductService brandLineProductService;

	@Override
	public BrandEntity toEntity(BrandDto dto) {
		if(dto!=null) {
			return mapper.map(dto, BrandEntity.class);
		}
		return null;
	}

	@Override
	public BrandDto toDto(BrandEntity entity) {
		if(entity!=null) {
			BrandDto dto = mapper.map(entity, BrandDto.class);
			if(entity.getId()!=null) {
				List<String> lineProducts = new ArrayList<>();
				brandLineProductService.findLineProductByBrandId(entity.getId()).forEach(lineProduct -> {
					lineProducts.add(lineProduct.getName());
				});
				dto.setLineProducts(lineProducts);
			}
			return dto;
		}
		return null;
	}

	@Override
	public List<BrandEntity> toEntity(List<BrandDto> dtos) {
		List<BrandEntity> entitys = new ArrayList<>();
		for(BrandDto dto : dtos) {
			entitys.add(toEntity(dto));
		}
		return entitys;
	}

	@Override
	public List<BrandDto> toDto(List<BrandEntity> entitys) {
		List<BrandDto> dtos = new ArrayList<>();
		for(BrandEntity entity : entitys) {
			dtos.add(toDto(entity));
		}
		return dtos;
	}

}
