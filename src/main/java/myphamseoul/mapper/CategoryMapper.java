package myphamseoul.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import myphamseoul.base.MapperBase;
import myphamseoul.dto.CategoryDto;
import myphamseoul.entity.CategoryEntity;
import myphamseoul.service.ProductMainService;

@Component
public class CategoryMapper implements MapperBase<CategoryEntity, CategoryDto>{
	
	@Autowired
	private ProductMainService productMainService;
	
	@Autowired
	private ProductMainMapper productMainMapper;

	@Override
	public CategoryEntity toEntity(CategoryDto dto) {
		if(dto!=null) {
			return mapper.map(dto, CategoryEntity.class);
		}
		return null;
	}

	@Override
	public CategoryDto toDto(CategoryEntity entity) {
		if(entity!=null) {
			CategoryDto dto = mapper.map(entity, CategoryDto.class);
			dto.setProductMains(productMainMapper.toDto(productMainService.findByCategory(entity)));
			return dto;
		}
		return null;
	}

	@Override
	public List<CategoryEntity> toEntity(List<CategoryDto> dtos) {
		return dtos.stream().map(dto -> {return toEntity(dto);}).collect(Collectors.toList());
	}

	@Override
	public List<CategoryDto> toDto(List<CategoryEntity> entitys) {
		return entitys.stream().map(entity -> {return toDto(entity);}).collect(Collectors.toList());
	}

}
