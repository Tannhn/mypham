package myphamseoul.dto;

import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class ProductMainDto extends ProductGeneral{
	private String imageMainId;
	private String imageHoverId;
	Set<ImageColorDto> imageColors = new HashSet<ImageColorDto>();
}
