package myphamseoul.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import myphamseoul.base.DtoBase;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto extends DtoBase{
	private Long id;
	private String name;
	private String description;
	private List<ProductMainDto> productMains = new ArrayList<ProductMainDto>();
}
