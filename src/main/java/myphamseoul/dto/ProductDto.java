package myphamseoul.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
	private ProductMainDto productMain;
	private ColorDto color;
	private PromotionDto promotion;
	List<PromotionDto> promotions;
	private Long price;
	private List<ImageDto> images = new ArrayList<ImageDto>();
	private List<String> imageIds = new ArrayList<String>();
}
