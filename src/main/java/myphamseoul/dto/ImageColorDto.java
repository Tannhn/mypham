package myphamseoul.dto;

import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImageColorDto {
	private ColorDto color;
	private Set<String> imageIds = new HashSet<String>();
}
