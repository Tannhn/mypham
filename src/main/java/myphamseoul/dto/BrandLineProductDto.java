package myphamseoul.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class BrandLineProductDto {
	private Long brandId;
	
	private String brandName;
	
	private String lineProductName;
}
