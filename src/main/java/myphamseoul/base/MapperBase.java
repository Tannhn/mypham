package myphamseoul.base;

import java.util.List;

import org.modelmapper.ModelMapper;

public interface MapperBase<E,D> {
	public ModelMapper mapper = new ModelMapper();
	E toEntity(D dto);
	D toDto(E entity);
	List<E> toEntity(List<D> dtos);
	List<D> toDto(List<E> entitys);
}
