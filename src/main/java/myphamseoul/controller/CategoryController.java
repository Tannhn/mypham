package myphamseoul.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myphamseoul.dto.CategoryDto;
import myphamseoul.mapper.CategoryMapper;
import myphamseoul.response.MessageResponse;
import myphamseoul.service.CategoryService;

@RestController
@RequestMapping("/api/v1/category")
public class CategoryController {
	
	private static final Logger log = LoggerFactory.getLogger(CategoryController.class);

	@Autowired
	private CategoryService categoryService;
	@Autowired
	private CategoryMapper categoryMapper;
	@PutMapping
	public ResponseEntity<?> putCategory(@RequestBody CategoryDto dto) {
		log.info("put category name: "+dto.getName());
		return ResponseEntity.ok().body(categoryMapper.toDto(categoryService.save(categoryMapper.toEntity(dto))));
	}
	
	@GetMapping("/all")
	public ResponseEntity<?> getCategoryAll() {
		log.info("get category all");
		return ResponseEntity.ok().body(categoryMapper.toDto(categoryService.getAll()));
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getCategoryById (@PathVariable Long id) {
		log.info("get category id: "+id);
		return ResponseEntity.ok().body(
				categoryService.existById(id)?
						categoryMapper.toDto(categoryService.getById(id)) :
						new MessageResponse("ID not exist")
				);
	}
}
