package myphamseoul.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myphamseoul.service.ProductPromotionService;

@RestController
@RequestMapping("/api/v1/product-promotion")
public class ProductPromotionController {
	@Autowired
	private ProductPromotionService productPromotionService;
	
	@GetMapping("/{productMainId}/{colorId}/{promotionId}")
	public ResponseEntity<?> getProductPromotionAll(
			@PathVariable String productMainId,
			@PathVariable Long colorId,
			@PathVariable Long promotionId
			) {
		return ResponseEntity.ok().body(productPromotionService.findByProductMainIdAndColorIdAndPromotionId(productMainId, colorId, promotionId));
	}

}
