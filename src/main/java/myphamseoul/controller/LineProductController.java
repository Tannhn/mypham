package myphamseoul.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myphamseoul.dto.LineProductDto;
import myphamseoul.mapper.LineProductMapper;
import myphamseoul.response.MessageResponse;
import myphamseoul.service.BrandLineProductService;
import myphamseoul.service.LineProductService;

@RestController
@RequestMapping("/api/v1/line-product")
public class LineProductController {

	private static final Logger log = LoggerFactory.getLogger(LineProductController.class);

	@Autowired
	private LineProductService lineProductService;
	@Autowired
	private LineProductMapper lineProductMapper;
	
	@Autowired
	private BrandLineProductService brandLineProductService;

	@GetMapping("/all")
	public ResponseEntity<?> getLineProductAll() {
		log.info("get line-product-all");
		return ResponseEntity.ok().body(lineProductMapper.toDto(lineProductService.getAll()));
	}
	
	@GetMapping(path = "/by-brand/{brandId}")
	public ResponseEntity<?> getLineProductByBrandId(@PathVariable Long brandId) {
		log.info("get getLineProductByBrandId");
		return ResponseEntity.ok().body(lineProductMapper.toDto(brandLineProductService.findLineProductByBrandId(brandId)));
	}

	@PutMapping
	public ResponseEntity<?> putMethodName(@RequestBody LineProductDto dto) {
		log.info("put line-product");
		dto.setName(dto.getName().trim());
		if (dto.getId() == null) {
			if (lineProductService.findOneByName(dto.getName()) != null) {
				return ResponseEntity.ok().body(new MessageResponse("Line product already exist!"));
			}
		}
		return ResponseEntity.ok()
				.body(lineProductMapper.toDto(lineProductService.save(lineProductMapper.toEntity(dto))));
	}

}
