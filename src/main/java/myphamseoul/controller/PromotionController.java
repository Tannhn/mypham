package myphamseoul.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myphamseoul.dto.PromotionDto;
import myphamseoul.entity.PromotionEntity;
import myphamseoul.mapper.PromotionMapper;
import myphamseoul.response.MessageResponse;
import myphamseoul.service.ProductPromotionService;
import myphamseoul.service.PromotionService;

@RestController
@RequestMapping(path = "/api/v1/promotion")
public class PromotionController {
	
	private static final Logger log = LoggerFactory.getLogger(PromotionController.class);
	
	@Autowired
	private PromotionService promotionService;
	
	@Autowired
	private PromotionMapper promotionMapper;
	
	@Autowired
	private ProductPromotionService productPromotionService;

	@PutMapping
	public ResponseEntity<?> putPromotion(@RequestBody PromotionDto dto) {
		log.info("putPromotion dto: "+dto.toString());
		if(dto!=null) {
			if(dto.getLocationIn()==null||dto.getLocationOut()==null) {
				ResponseEntity.ok().body(new MessageResponse("date illegal!"));
			}else {
				return ResponseEntity.ok().body(promotionMapper.toDto(promotionService.save(promotionMapper.toEntity(dto))));
			}
		}
		return ResponseEntity.ok().body(new MessageResponse("failed!"));
	}
	
	@GetMapping(value = "/by-product/{productMainId}/{colorId}")
	public ResponseEntity<?> getMethodName(@PathVariable String productMainId,@PathVariable Long colorId) {
		List<PromotionEntity> promos = productPromotionService.findPromotionByProductProductMainIdAndProductColorId(productMainId, colorId);
		return ResponseEntity.ok().body(promos);
	}
	
	@GetMapping(value = "/by-product/{productMainId}")
	public ResponseEntity<?> getPromotionByProductMainId(@PathVariable String productMainId) {
		List<PromotionEntity> promos = promotionService.findPromotionByProductProductMainId(productMainId);
		return ResponseEntity.ok().body(promos);
	}


}
