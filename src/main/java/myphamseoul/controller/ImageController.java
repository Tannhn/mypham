package myphamseoul.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import myphamseoul.entity.ImageEntity;
import myphamseoul.mapper.ImageMapper;
import myphamseoul.response.MessageResponse;
import myphamseoul.service.FileService;
import myphamseoul.service.ImageService;

@RestController
@RequestMapping("/api/v1/img")
public class ImageController {
	
	private static final Logger log = LoggerFactory.getLogger(ImageController.class);

	@Autowired
	private ImageService imageService;
	
	@Autowired
	private ImageMapper imageMapper;
	
	@Autowired
	private FileService fileService;
	@PostMapping
	public ResponseEntity<?> postMethodName(@RequestBody MultipartFile file) {
		if(file.getContentType().indexOf("image")!=-1) {
			ImageEntity imgE = new ImageEntity();
			String filename = fileService.save(file);
			imgE.setName(filename);
			imgE.setDraft(true);
			imgE = imageService.save(imgE);
			return ResponseEntity.ok().body(imageMapper.toDto(imgE));
		}
		return ResponseEntity.ok().body(new MessageResponse("Sai định dạng file"));
	}
	
	@DeleteMapping(value = "/delete-name/{name}")
	public ResponseEntity<?> delImageByName(@PathVariable String name) {
		log.info("delImageByName: "+name);
		if(fileService.deleteByFilename(name)) {
			return ResponseEntity.ok().body(true);
		}
		return ResponseEntity.ok().body(false);
	}
	
	@DeleteMapping(value = "/delete-id/{id}")
	public ResponseEntity<?> delImageById(@PathVariable String id) {
		log.info("delImageById: "+id);
		if(imageService.existById(id)) {
			ImageEntity imgE = imageService.getById(id);
			if(fileService.deleteByFilename(imgE.getName())) {
				imageService.delete(id);
				return ResponseEntity.ok().body(true);
			}
		}
		return ResponseEntity.ok().body(false);
	}
	
	@GetMapping(value = "/by-productmain/{productMainId}")
	public ResponseEntity<?> getImageByProductMainId(@PathVariable String productMainId) {
		log.info("getImageByProductMainId: "+productMainId);
		return ResponseEntity.ok().body(imageService.findByProductMainId(productMainId));
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getImageById(@PathVariable String id) {
		log.info("getImageById: "+id);
		if(id!=null&&imageService.existById(id)) {
			ImageEntity imageE = imageService.getById(id);
			Resource resource = fileService.getResource(imageE.getName());
			String contentType = fileService.getContentTypeByFilename(imageE.getName());
			if(resource!=null&&contentType!=null) {
				return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType)).body(resource);
			}
		}
		return ResponseEntity.ok().body(null);
	}


}
