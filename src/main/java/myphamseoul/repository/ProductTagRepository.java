package myphamseoul.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import myphamseoul.entity.ProductMainEntity;
import myphamseoul.entity.ProductTagEntity;
import myphamseoul.entity.ProductTagkey;
import myphamseoul.entity.TagEntity;

public interface ProductTagRepository extends JpaRepository<ProductTagEntity, ProductTagkey>{
	@Query(value = "select pt.productMain from ProductTagEntity pt where pt.tag = ?1")
	List<ProductMainEntity> findProductMainByTag(TagEntity tag);
	@Query(value = "select pt.tag from ProductTagEntity pt where pt.productMain = ?1")
	List<TagEntity> findProductMainByProductMain(ProductMainEntity productMain);
	ProductTagEntity findByProductMainIdAndTagId(String productMainId,Long tagId);
}
