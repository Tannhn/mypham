package myphamseoul.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import myphamseoul.entity.ColorEntity;

public interface ColorRepository extends JpaRepository<ColorEntity, Long>{
	ColorEntity findByNameIgnoreCaseAndCodeIgnoreCase(String name, String code);
}
