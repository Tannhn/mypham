package myphamseoul.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import myphamseoul.entity.CategoryEntity;
import myphamseoul.entity.ProductMainEntity;

public interface ProductMainRepository extends JpaRepository<ProductMainEntity, String>{
	List<ProductMainEntity> findByCategory(CategoryEntity categoryE);
	ProductMainEntity findBySku(String sku);
}
