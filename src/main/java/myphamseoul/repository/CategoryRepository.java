package myphamseoul.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import myphamseoul.entity.CategoryEntity;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {
	CategoryEntity findOneByNameIgnoreCase(String name);
}
