package myphamseoul.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import myphamseoul.entity.ProductMainEntity;
import myphamseoul.entity.ProductMainPromotionEntity;
import myphamseoul.entity.ProductMainPromotionKey;
import myphamseoul.entity.PromotionEntity;

public interface ProductMainPromotionRepository extends JpaRepository<ProductMainPromotionEntity, ProductMainPromotionKey>{
	List<ProductMainEntity> findProductMainByPromotion(PromotionEntity promotion);
}
