package myphamseoul.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import myphamseoul.entity.TagEntity;

public interface TagRepository extends JpaRepository<TagEntity, Long>{
	TagEntity findOneByNameIgnoreCase(String name);
}
