package myphamseoul.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import myphamseoul.entity.ImageEntity;
import myphamseoul.entity.ProductEntity;

public interface ImageRepository extends JpaRepository<ImageEntity, String> {
	@Query(value = "select i from ImageEntity i where i.product = ?1 and i.main = 1")
	ImageEntity findImgMainByProductColor(ProductEntity product);
	@Query(value = "select i from ImageEntity i where i.product = ?1 and i.hover = 1")
	ImageEntity findImgHoverByProductColor(ProductEntity product);
	@Query(value = "select i.* from image i where i.productmain_id = :productMainId",nativeQuery = true)
	List<ImageEntity> findByProductMainId(@Param("productMainId") String productMainId);
	List<ImageEntity> findByProduct(ProductEntity product);
	boolean existsByName(String name);
	ImageEntity findByName(String name);
}
