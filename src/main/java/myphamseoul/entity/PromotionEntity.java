package myphamseoul.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import myphamseoul.base.EntityBase;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = "promotion")
public class PromotionEntity extends EntityBase{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(length = 255)
	private String name;
	@Column(columnDefinition = "LONGTEXT")
	private String description;
	@Column(name = "location_in",nullable = false)
	private Date locationIn;
	@Column(name = "location_out",nullable = false)
	private Date locationOut;
	private int sale;
}
