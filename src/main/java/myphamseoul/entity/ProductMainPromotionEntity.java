package myphamseoul.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "product_main_promotion")
public class ProductMainPromotionEntity {
	@EmbeddedId
	private ProductMainPromotionKey id;
	@ManyToOne
	@JoinColumn(name = "productmain_id")
	@MapsId(value = "productMainId")
	private ProductMainEntity productMain;
	@ManyToOne
	@JoinColumn(name = "promotion_id")
	@MapsId(value = "promotionId")
	private PromotionEntity promotion;
}
