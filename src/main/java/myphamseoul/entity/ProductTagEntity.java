package myphamseoul.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "product_tag")
public class ProductTagEntity {
	@EmbeddedId
	private ProductTagkey id;
	
	@ManyToOne
	@MapsId(value = "productMainId")
	@JoinColumn(name = "productmain_id")
	private ProductMainEntity productMain;
	@ManyToOne
	@MapsId(value = "tagId")
	@JoinColumn(name = "tag_id")
	private TagEntity tag;
}
