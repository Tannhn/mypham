package myphamseoul.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductTagkey implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "productmain_id")
	private String productMainId;
	@Column(name = "tag_id")
	private Long tagId;
}
