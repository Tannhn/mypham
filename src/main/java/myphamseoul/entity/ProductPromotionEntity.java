package myphamseoul.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = "product_promotion")
public class ProductPromotionEntity{
	@EmbeddedId
	private ProductPromotionKey id;
	@ManyToOne
	@JoinColumns(value = {@JoinColumn(name = "color_id"),@JoinColumn(name = "productmain_id")})
	@MapsId(value = "productColorId")
	private ProductEntity product;
	@ManyToOne
	@JoinColumn(name = "promotion_id")
	@MapsId(value = "promotionId")
	private PromotionEntity promotion;
}
