package myphamseoul.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class BrandLineProductKey implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name = "brand_id")
	private Long brandId;
	
	@Column(name = "lineproduct_id")
	private Long lineProductId;
}
