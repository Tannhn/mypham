package myphamseoul.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductPromotionKey implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "product")
	private ProductColorKey productColorId;
	@Column(name = "promotion_id")
	private Long promotionId;

}
