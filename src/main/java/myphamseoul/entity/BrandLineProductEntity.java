package myphamseoul.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "brand_lineproduct")
public class BrandLineProductEntity {
	@EmbeddedId
	private BrandLineProductKey id;
	
	@ManyToOne
	@MapsId(value = "brandId")
	@JoinColumn(name = "brand_id")
	private BrandEntity brand;
	
	@ManyToOne
	@MapsId(value = "lineProductId")
	@JoinColumn(name = "lineproduct_id")
	private LineProductEntity lineProduct;
}
