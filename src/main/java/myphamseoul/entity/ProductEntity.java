package myphamseoul.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import myphamseoul.base.EntityBase;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product")
public class ProductEntity extends EntityBase {
	@EmbeddedId
	private ProductColorKey id;
	@ManyToOne
	@MapsId(value = "productMainId")
	@JoinColumn(name = "productmain_id")
	private ProductMainEntity productMain;
	@ManyToOne
	@MapsId(value = "colorId")
	@JoinColumn(name = "color_id")
	private ColorEntity color;
	
	private long price;
}
