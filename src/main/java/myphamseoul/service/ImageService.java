package myphamseoul.service;

import java.util.List;

import myphamseoul.base.ServiceBase;
import myphamseoul.entity.ImageEntity;
import myphamseoul.entity.ProductEntity;

public interface ImageService extends ServiceBase<ImageEntity, String>{
	ImageEntity findImgMainByProductMainIdAndColorId(String productMainId, Long colorId);
	ImageEntity findImgHoverByProductMainIdAndColorId(String productMainId, Long colorId);
	List<ImageEntity> findByProductMainId(String productMainId);
	List<ImageEntity> findByProduct(ProductEntity product);
	boolean existsByName(String name);
	ImageEntity findByName(String name);
}
