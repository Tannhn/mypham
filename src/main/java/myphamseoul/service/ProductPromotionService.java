package myphamseoul.service;

import java.util.List;

import myphamseoul.base.ServiceBase;
import myphamseoul.entity.ProductEntity;
import myphamseoul.entity.ProductPromotionEntity;
import myphamseoul.entity.ProductPromotionKey;
import myphamseoul.entity.PromotionEntity;

public interface ProductPromotionService extends ServiceBase<ProductPromotionEntity, ProductPromotionKey>{
	ProductPromotionEntity findByProductMainIdAndColorIdAndPromotionId(String productMainId, Long colorId, Long promotionId);
	List<PromotionEntity> findPromotionByProductProductMainIdAndProductColorId(String productMainId,Long colorId);
	List<ProductEntity> findProductByPromotionId(Long promotionId);
}
