package myphamseoul.service;

import myphamseoul.base.ServiceBase;
import myphamseoul.entity.BrandEntity;

public interface BrandService extends ServiceBase<BrandEntity, Long>{
	BrandEntity findOneByName(String name);
}
