package myphamseoul.service;

import java.util.List;

import myphamseoul.base.ServiceBase;
import myphamseoul.dto.ProductMainDto;
import myphamseoul.entity.CategoryEntity;
import myphamseoul.entity.ProductMainEntity;

public interface ProductMainService extends ServiceBase<ProductMainEntity, String>{
	List<ProductMainEntity> findByCategory(CategoryEntity categoryE);
	ProductMainEntity save(ProductMainDto dto);
	List<ProductMainEntity> save(List<ProductMainDto> dtos);
	List<ProductMainEntity> findProductMainByPromotionId(Long promotionId);
	ProductMainEntity findBySku(String sku);
}
