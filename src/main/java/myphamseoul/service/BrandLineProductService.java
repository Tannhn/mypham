package myphamseoul.service;

import java.util.List;

import myphamseoul.base.ServiceBase;
import myphamseoul.dto.BrandLineProductDto;
import myphamseoul.entity.BrandEntity;
import myphamseoul.entity.BrandLineProductEntity;
import myphamseoul.entity.BrandLineProductKey;
import myphamseoul.entity.LineProductEntity;

public interface BrandLineProductService extends ServiceBase<BrandLineProductEntity, BrandLineProductKey> {
	BrandLineProductEntity save(BrandLineProductDto dto);
	List<LineProductEntity> findLineProductByBrandId(Long brandId);
	List<BrandEntity> findBrandByLineProductId(Long lineProductId);
	BrandLineProductEntity findByBrandIdAndLineProductId(Long brandId,Long lineProductId);
}
