package myphamseoul.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.entity.CategoryEntity;
import myphamseoul.repository.CategoryRepository;
import myphamseoul.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService{
	
	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public List<CategoryEntity> getAll() {
		return categoryRepository.findAll();
	}

	@Override
	public CategoryEntity getById(Long id) {
		return categoryRepository.getOne(id);
	}

	@Override
	public CategoryEntity save(CategoryEntity entity) {
		return categoryRepository.save(entity);
	}

	@Override
	public boolean existById(Long id) {
		return categoryRepository.existsById(id);
	}

	@Override
	public void delete(Long id) {
		categoryRepository.deleteById(id);
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids) {
			delete(id);
		}
	}

	@Override
	public CategoryEntity findOneByName(String name) {
		return categoryRepository.findOneByNameIgnoreCase(name.trim());
	}

}
