package myphamseoul.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.dto.BrandLineProductDto;
import myphamseoul.dto.ProductMainDto;
import myphamseoul.dto.ProductTagDto;
import myphamseoul.entity.BrandLineProductEntity;
import myphamseoul.entity.CategoryEntity;
import myphamseoul.entity.ImageEntity;
import myphamseoul.entity.ProductMainEntity;
import myphamseoul.entity.PromotionEntity;
import myphamseoul.mapper.ProductMainMapper;
import myphamseoul.repository.ProductMainPromotionRepository;
import myphamseoul.repository.ProductMainRepository;
import myphamseoul.service.BrandLineProductService;
import myphamseoul.service.CategoryService;
import myphamseoul.service.ImageService;
import myphamseoul.service.ProductMainService;
import myphamseoul.service.ProductTagService;
import myphamseoul.service.PromotionService;

@Service
public class ProductMainServiceImpl implements ProductMainService {

	@Autowired
	private ProductMainRepository productMainRepository;

	@Autowired
	private ProductMainPromotionRepository productMainPromotionRepository;

	@Autowired
	private PromotionService promotionService;

	@Autowired
	private ProductMainMapper productMainMapper;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private BrandLineProductService brandLindProductService;

	@Autowired
	private ProductTagService productTagService;

	@Autowired
	private ImageService imageService;

	@Override
	public List<ProductMainEntity> getAll() {
		return productMainRepository.findAll();
	}

	@Override
	public ProductMainEntity getById(String id) {
		return productMainRepository.getOne(id);
	}

	@Override
	public ProductMainEntity save(ProductMainEntity entity) {
		return productMainRepository.save(entity);
	}

	@Override
	public boolean existById(String id) {
		return productMainRepository.existsById(id);
	}

	@Override
	public void delete(String id) {
		productMainRepository.deleteById(id);
	}

	@Override
	public void delete(String[] ids) {
		for (String id : ids) {
			delete(id);
		}
	}

	@Override
	public List<ProductMainEntity> findByCategory(CategoryEntity categoryE) {
		return productMainRepository.findByCategory(categoryE);
	}

	@Override
	public ProductMainEntity save(ProductMainDto dto) {
		if (dto != null
				&& dto.getCategoryName() != null) {
			ProductMainEntity proMainE = productMainMapper.toEntity(dto);
			if (proMainE.getId() != null && !proMainE.getId().trim().equals("")) {
				if (!existById(proMainE.getId())) {
					return null;
				}
			}
			if (proMainE.getCategory().getId() == null) {
				CategoryEntity catE = categoryService
						.save(new CategoryEntity(null, dto.getCategoryName().trim(), null));
				proMainE.setCategory(catE);
			}
			if (proMainE.getBrandLineProduct() == null) {
				BrandLineProductDto blpD = new BrandLineProductDto(dto.getBrandId(), dto.getBrandName(), dto.getLineProductName());
				BrandLineProductEntity brandLineProE = brandLindProductService.save(blpD);
				if(brandLineProE!=null) {
					proMainE.setBrandLineProduct(brandLineProE);
				}
				
			}
			ProductMainEntity proSku = findBySku(proMainE.getSku().trim());
			if (proSku != null && proSku.getId() != proMainE.getId()) {
				return null;
			}
			proMainE = productMainRepository.save(proMainE);
			if (dto.getTags() != null && dto.getTags().size() > 0) {
				for (String tag : dto.getTags()) {
					productTagService.save(new ProductTagDto(proMainE.getId(), tag));
				}
			}
			if (dto.getImageMainId() != null && !dto.getImageMainId().trim().equals("")) {
				if (imageService.existById(dto.getImageMainId())) {
					ImageEntity imgMain = imageService.getById(dto.getImageMainId());
					if (!imgMain.isMain()) {
						List<ImageEntity> imgs = imageService.findByProductMainId(proMainE.getId());
						for (ImageEntity img : imgs) {
							img.setMain(false);
							imageService.save(img);
						}
						imgMain.setMain(true);
						imageService.save(imgMain);
					}
				}
			}
			if (dto.getImageHoverId() != null && !dto.getImageHoverId().trim().equals("")) {
				if (imageService.existById(dto.getImageHoverId())) {
					ImageEntity imgHover = imageService.getById(dto.getImageHoverId());
					if (!imgHover.isHover()) {

						List<ImageEntity> imgs = imageService.findByProductMainId(proMainE.getId());
						for (ImageEntity img : imgs) {
							img.setHover(false);
							imageService.save(img);
						}
						imgHover.setMain(true);
						imageService.save(imgHover);
					}
				}
			}

			return proMainE;
		}
		return null;
	}

	@Override
	public List<ProductMainEntity> findProductMainByPromotionId(Long promotionId) {
		PromotionEntity promoE = promotionService.getById(promotionId);
		if (promoE != null) {
			return productMainPromotionRepository.findProductMainByPromotion(promoE);
		}
		return null;
	}

	@Override
	public ProductMainEntity findBySku(String sku) {
		return productMainRepository.findBySku(sku);
	}

	@Override
	public List<ProductMainEntity> save(List<ProductMainDto> dtos) {
		List<ProductMainEntity> entitys = new ArrayList<>();
		for (ProductMainDto dto : dtos) {
			entitys.add(save(dto));
		}
		return entitys;
	}

}
