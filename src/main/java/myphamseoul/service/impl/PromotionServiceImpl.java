package myphamseoul.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.entity.PromotionEntity;
import myphamseoul.repository.ProductPromotionRepository;
import myphamseoul.repository.PromotionRepository;
import myphamseoul.service.ProductMainService;
import myphamseoul.service.PromotionService;

@Service
public class PromotionServiceImpl implements PromotionService{
	
	@Autowired
	private ProductPromotionRepository productPromotionRepository;
	
	@Autowired
	private ProductMainService productMainService;
	
	@Autowired
	private PromotionRepository promotionRepository;

	@Override
	public List<PromotionEntity> getAll() {
		return promotionRepository.findAll();
	}

	@Override
	public PromotionEntity getById(Long id) {
		return promotionRepository.getOne(id);
	}

	@Override
	public PromotionEntity save(PromotionEntity entity) {
		return promotionRepository.save(entity);
	}

	@Override
	public boolean existById(Long id) {
		return promotionRepository.existsById(id);
	}

	@Override
	public void delete(Long id) {
		promotionRepository.deleteById(id);
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids) {
			delete(id);
		}
	}
	
	@Override
	public List<PromotionEntity> findPromotionByProductProductMainId(String productProductMainId) {
		Date dateNow = new Date();
		return productPromotionRepository.findPromotionByProductMain(productMainService.getById(productProductMainId),dateNow);
	}

}
