package myphamseoul.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import myphamseoul.service.FileService;

@Component
public class FileServiceImpl implements FileService{
	
	@Value("${app.file.location-dir}")
	private String pathDir;
	
	@Override
	public String getDir() {
		List<String> pathStrs = Arrays.asList(this.pathDir.split("[\\/]"));
		return String.join(File.separator, pathStrs);
	}

	@Override
	public String save(MultipartFile file) {
		Date date = new Date();
		String filename = file.getOriginalFilename();
		String[] filenameArr = filename.split("[.]");
		String name = filenameArr[0]+"-"+date.getTime()+"."+filenameArr[1];
		File path = new File(getDir()+File.separator+name);
		if(!path.canWrite()) {
			path.getParentFile().mkdir();
		}
		try {
			Files.copy(file.getInputStream(), path.toPath(), StandardCopyOption.REPLACE_EXISTING);
			return name;
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public Resource getResource(String filename) {
		Path path = Paths.get(getDir()).resolve(filename);
		try {
			Resource resource = new UrlResource(path.toUri());
			return resource;
		} catch (MalformedURLException e) {
			return null;
		}
	}

	@Override
	public String getContentTypeByFilename(String filename) {
		Path path = getFileByFilename(filename).toPath();
		String contentType;
		try {
			contentType = Files.probeContentType(path);
			return contentType;
		} catch (IOException e) {
			return null;
		}
		
	}

	@Override
	public boolean deleteByFilename(String filename) {
		File file = getFileByFilename(filename);
		return file.delete();
	}

	@Override
	public File getFileByFilename(String filename) {
		return new File(getDir()+File.separator+filename);
	}

}
