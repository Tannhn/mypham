package myphamseoul.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.dto.ProductTagDto;
import myphamseoul.entity.ProductMainEntity;
import myphamseoul.entity.ProductTagEntity;
import myphamseoul.entity.ProductTagkey;
import myphamseoul.entity.TagEntity;
import myphamseoul.repository.ProductTagRepository;
import myphamseoul.service.ProductMainService;
import myphamseoul.service.ProductTagService;
import myphamseoul.service.TagService;


@Service
public class ProductTagServiceImpl implements ProductTagService{
	
	@Autowired
	private ProductTagRepository productTagRepository;
	
	@Autowired
	private ProductMainService productMainService;
	
	@Autowired
	private TagService tagService;

	@Override
	public List<ProductTagEntity> getAll() {
		return productTagRepository.findAll();
	}

	@Override
	public ProductTagEntity getById(ProductTagkey id) {
		return productTagRepository.getOne(id);
	}

	@Override
	public ProductTagEntity save(ProductTagEntity entity) {
		return productTagRepository.save(entity);
	}

	@Override
	public boolean existById(ProductTagkey id) {
		return productTagRepository.existsById(id);
	}

	@Override
	public void delete(ProductTagkey id) {
		productTagRepository.deleteById(id);
	}

	@Override
	public void delete(ProductTagkey[] ids) {
		for(ProductTagkey id : ids) {
			delete(id);
		}
	}

	@Override
	public List<ProductMainEntity> findProductMainByTagId(Long tagId) {
		if(tagService.existById(tagId)) {
			TagEntity tagE = tagService.getById(tagId);
			return productTagRepository.findProductMainByTag(tagE);
		}
		return null;
	}

	@Override
	public List<TagEntity> findProductMainByProductMainId(String productMainId) {
		if(productMainService.existById(productMainId)) {
			ProductMainEntity productMainE = productMainService.getById(productMainId);
			return productTagRepository.findProductMainByProductMain(productMainE);
		}
		return null;
	}

	@Override
	public ProductTagEntity findByProductMainIdAndTagId(String productMainId, Long tagId) {
		if(tagService.existById(tagId)&&productMainService.existById(productMainId)) {
			return productTagRepository.findByProductMainIdAndTagId(productMainId, tagId);
		}
		return null;
	}

	@Override
	public ProductTagEntity save(ProductTagDto tagD) {
		if(tagD!=null) {
			ProductMainEntity proMainE = productMainService.getById(tagD.getProductMainId());
			TagEntity tagE = tagService.findOneByName(tagD.getTagName());
			if(tagE==null) {
				tagE = tagService.save(new TagEntity(null, tagD.getTagName().trim(), null));
			}
			if(proMainE!=null&&tagE!=null) {
				return save(new ProductTagEntity(new ProductTagkey(proMainE.getId(), tagE.getId()), proMainE, tagE));
			}
		}
		return null;
	}

}
