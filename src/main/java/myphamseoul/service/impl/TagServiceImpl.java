package myphamseoul.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.entity.TagEntity;
import myphamseoul.repository.TagRepository;
import myphamseoul.service.TagService;

@Service
public class TagServiceImpl implements TagService{
	
	@Autowired
	private TagRepository tagRepository;

	@Override
	public List<TagEntity> getAll() {
		return tagRepository.findAll();
	}

	@Override
	public TagEntity getById(Long id) {
		return tagRepository.getOne(id);
	}

	@Override
	public TagEntity save(TagEntity entity) {
		return tagRepository.save(entity);
	}

	@Override
	public void delete(Long id) {
		tagRepository.deleteById(id);
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids) {
			delete(id);
		}
	}

	@Override
	public TagEntity findOneByName(String name) {
		return tagRepository.findOneByNameIgnoreCase(name);
	}

	@Override
	public boolean existById(Long id) {
		return tagRepository.existsById(id);
	}

}
