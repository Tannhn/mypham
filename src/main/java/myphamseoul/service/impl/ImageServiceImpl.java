package myphamseoul.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.entity.ImageEntity;
import myphamseoul.entity.ProductColorKey;
import myphamseoul.entity.ProductEntity;
import myphamseoul.repository.ImageRepository;
import myphamseoul.service.ImageService;
import myphamseoul.service.ProductService;

@Service
public class ImageServiceImpl implements ImageService {
	
	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private ProductService productService;

	@Override
	public List<ImageEntity> getAll() {
		return imageRepository.findAll();
	}

	@Override
	public ImageEntity getById(String id) {
		return imageRepository.getOne(id);
	}

	@Override
	public ImageEntity save(ImageEntity entity) {
		return imageRepository.save(entity);
	}

	@Override
	public void delete(String id) {
		imageRepository.deleteById(id);
	}

	@Override
	public void delete(String[] ids) {
		for(String id : ids) {
			delete(id);
		}
	}

	@Override
	public boolean existById(String id) {
		return imageRepository.existsById(id);
	}

	@Override
	public ImageEntity findImgMainByProductMainIdAndColorId(String productMainId, Long colorId) {
		return imageRepository.findImgMainByProductColor(productService.getById(new ProductColorKey(productMainId,colorId)));
	}

	@Override
	public ImageEntity findImgHoverByProductMainIdAndColorId(String productMainId, Long colorId) {
		return imageRepository.findImgHoverByProductColor(productService.getById(new ProductColorKey(productMainId,colorId)));
	}

	@Override
	public List<ImageEntity> findByProductMainId(String productMainId) {
		return imageRepository.findByProductMainId(productMainId);
	}

	@Override
	public List<ImageEntity> findByProduct(ProductEntity product) {
		return imageRepository.findByProduct(product);
	}

	@Override
	public boolean existsByName(String name) {
		return imageRepository.existsByName(name);
	}

	@Override
	public ImageEntity findByName(String name) {
		return imageRepository.findByName(name);
	}

}
