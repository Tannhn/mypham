package myphamseoul.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.entity.LineProductEntity;
import myphamseoul.repository.LineProductRepository;
import myphamseoul.service.LineProductService;

@Service
public class LineProductServiceImpl implements LineProductService {
	
	@Autowired
	private LineProductRepository lineProductRepository;

	@Override
	public List<LineProductEntity> getAll() {
		return lineProductRepository.findAll();
	}

	@Override
	public LineProductEntity getById(Long id) {
		return lineProductRepository.getOne(id);
	}

	@Override
	public LineProductEntity save(LineProductEntity entity) {
		return lineProductRepository.save(entity);
	}

	@Override
	public void delete(Long id) {
		lineProductRepository.deleteById(id);
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids) {
			delete(id);
		}
	}

	@Override
	public LineProductEntity findOneByName(String name) {
		return lineProductRepository.findByNameIgnoreCase(name);
	}

	@Override
	public boolean existById(Long id) {
		return lineProductRepository.existsById(id);
	}

}
