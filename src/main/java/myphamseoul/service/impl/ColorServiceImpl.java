package myphamseoul.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.entity.ColorEntity;
import myphamseoul.repository.ColorRepository;
import myphamseoul.service.ColorService;

@Service
public class ColorServiceImpl implements ColorService{
	
	@Autowired
	private ColorRepository colorRepository;

	@Override
	public List<ColorEntity> getAll() {
		return colorRepository.findAll();
	}

	@Override
	public ColorEntity getById(Long id) {
		return colorRepository.getOne(id);
	}

	@Override
	public ColorEntity save(ColorEntity entity) {
		return colorRepository.save(entity);
	}

	@Override
	public boolean existById(Long id) {
		return colorRepository.existsById(id);
	}

	@Override
	public void delete(Long id) {
		colorRepository.deleteById(id);
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids) {
			delete(id);
		}
	}

	@Override
	public ColorEntity findByNameAndCode(String name, String code) {
		if(name!=null&&name.trim()!=""&&code!=null&&code.trim()!="") {
			return colorRepository.findByNameIgnoreCaseAndCodeIgnoreCase(name, code);
		}
		return null;
	}

}
