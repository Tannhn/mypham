package myphamseoul.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.entity.ProductColorKey;
import myphamseoul.entity.ProductEntity;
import myphamseoul.entity.ProductPromotionEntity;
import myphamseoul.entity.ProductPromotionKey;
import myphamseoul.entity.PromotionEntity;
import myphamseoul.repository.ProductPromotionRepository;
import myphamseoul.service.ProductPromotionService;
import myphamseoul.service.ProductService;

@Service
public class ProductPromotionServiceImpl implements ProductPromotionService {
	
	@Autowired
	private ProductPromotionRepository productPromotionRepository;
	
	@Autowired
	private ProductService productService;

	@Override
	public List<ProductPromotionEntity> getAll() {
		return productPromotionRepository.findAll();
	}

	@Override
	public ProductPromotionEntity getById(ProductPromotionKey id) {
		return productPromotionRepository.findByProductProductMainIdAndProductColorIdAndPromotionId(id.getProductColorId().getProductMainId(),
					id.getProductColorId().getColorId(),
					id.getPromotionId()
				);
	}

	@Override
	public ProductPromotionEntity save(ProductPromotionEntity entity) {
		return productPromotionRepository.save(entity);
	}

	@Override
	public boolean existById(ProductPromotionKey id) {
		return productPromotionRepository.existsById(id);
	}

	@Override
	public void delete(ProductPromotionKey id) {
		productPromotionRepository.deleteById(id);
	}

	@Override
	public void delete(ProductPromotionKey[] ids) {
		for(ProductPromotionKey id : ids) {
			delete(id);
		}
	}

	@Override
	public ProductPromotionEntity findByProductMainIdAndColorIdAndPromotionId(String productMainId, Long colorId,
			Long promotionId) {
		return productPromotionRepository.findByProductProductMainIdAndProductColorIdAndPromotionId(productMainId, colorId, promotionId);
	}

	@Override
	public List<PromotionEntity> findPromotionByProductProductMainIdAndProductColorId(String productMainId,
			Long colorId) {
		if(productMainId!=null && colorId!=null) {
			ProductEntity product = productService.getById(new ProductColorKey(productMainId, colorId));
			if(product!=null) {
				return productPromotionRepository.findPromotionByProduct(product);
			}
		}
		return null;
	}

	@Override
	public List<ProductEntity> findProductByPromotionId(Long promotionId) {
		return productPromotionRepository.findProductByPromotionId(promotionId);
	}

}
