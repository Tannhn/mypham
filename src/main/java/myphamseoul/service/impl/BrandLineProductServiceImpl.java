package myphamseoul.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.dto.BrandLineProductDto;
import myphamseoul.entity.BrandEntity;
import myphamseoul.entity.BrandLineProductEntity;
import myphamseoul.entity.BrandLineProductKey;
import myphamseoul.entity.LineProductEntity;
import myphamseoul.repository.BrandLineProductRepository;
import myphamseoul.service.BrandLineProductService;
import myphamseoul.service.BrandService;
import myphamseoul.service.LineProductService;

@Service
public class BrandLineProductServiceImpl implements BrandLineProductService {
	
	@Autowired
	private BrandService brandService;
	
	@Autowired
	private LineProductService lineProductService;
	
	@Autowired
	private BrandLineProductRepository brandLineProductRepository;

	@Override
	public List<BrandLineProductEntity> getAll() {
		return brandLineProductRepository.findAll();
	}

	@Override
	public BrandLineProductEntity getById(BrandLineProductKey id) {
		return brandLineProductRepository.getOne(id);
	}

	@Override
	public BrandLineProductEntity save(BrandLineProductEntity entity) {
		return brandLineProductRepository.save(entity);
	}
	@Override
	public BrandLineProductEntity save(BrandLineProductDto dto) {
		String lineProName = dto.getLineProductName()!=null?dto.getLineProductName().trim():null;
		String brandName = dto.getBrandName()!=null?dto.getBrandName().trim():null;
		BrandEntity brandE = new BrandEntity();
		if(lineProName != null && lineProName != "") {
			if(brandName != null && brandName != "") {
				if(dto.getBrandId()!=null&&brandService.existById(dto.getBrandId())) {
					brandE = brandService.getById(dto.getBrandId());
					if(!brandE.getName().trim().equalsIgnoreCase(brandName)) {
						brandE.setName(brandName.trim());
						brandE = brandService.save(brandE);
					}
				}else if(brandName != "") {
					brandE = brandService.findOneByName(brandName);
					if(brandE==null) {
						brandE = brandService.save(new BrandEntity(null, brandName));
					}
				}
			}else {
				brandE = brandService.getById(dto.getBrandId());
			}
			if(brandE!=null) {
				LineProductEntity lineproductE = lineProductService.findOneByName(dto.getLineProductName());
				if(lineproductE==null) {
					lineproductE = lineProductService.save(new LineProductEntity(null, dto.getLineProductName()));
				}
				return brandLineProductRepository.save(
						new BrandLineProductEntity(
								new BrandLineProductKey(brandE.getId(),
										lineproductE.getId()),
								brandE,
								lineproductE));
			}
		}
		return null;
	}

	@Override
	public void delete(BrandLineProductKey id) {
		brandLineProductRepository.deleteById(id);
	}

	@Override
	public void delete(BrandLineProductKey[] ids) {
		for(BrandLineProductKey id : ids) {
			delete(id);
		}
	}

	@Override
	public boolean existById(BrandLineProductKey id) {
		return brandLineProductRepository.existsById(id);
	}

	@Override
	public BrandLineProductEntity findByBrandIdAndLineProductId(Long brandId, Long lineProductId) {
		if(brandService.existById(lineProductId)&&lineProductService.existById(lineProductId)) {
			return brandLineProductRepository.findByBrandIdAndLineProductId(brandId, lineProductId);
		}
		return null;
	}

	@Override
	public List<LineProductEntity> findLineProductByBrandId(Long brandId) {
		BrandEntity brandE = brandService.getById(brandId);
		if(brandE!=null) {
			return brandLineProductRepository.findByBrand(brandE);
		}
		return null;
	}

	@Override
	public List<BrandEntity> findBrandByLineProductId(Long lineProductId) {
		LineProductEntity lineProductE = lineProductService.getById(lineProductId);
		if(lineProductE!=null) {
			return brandLineProductRepository.findByLineProduct(lineProductE);
		}
		return null;
	}

}
