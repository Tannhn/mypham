package myphamseoul.service;

import myphamseoul.base.ServiceBase;
import myphamseoul.entity.ColorEntity;

public interface ColorService extends ServiceBase<ColorEntity, Long>{
	ColorEntity findByNameAndCode(String name, String code);
}
