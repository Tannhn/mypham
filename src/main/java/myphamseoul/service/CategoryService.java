package myphamseoul.service;

import myphamseoul.base.ServiceBase;
import myphamseoul.entity.CategoryEntity;

public interface CategoryService extends ServiceBase<CategoryEntity, Long>{
	CategoryEntity findOneByName(String name);
}
