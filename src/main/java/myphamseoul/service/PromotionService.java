package myphamseoul.service;

import java.util.List;

import myphamseoul.base.ServiceBase;
import myphamseoul.entity.PromotionEntity;

public interface PromotionService extends ServiceBase<PromotionEntity, Long>{
	List<PromotionEntity> findPromotionByProductProductMainId(String productProductMainId);
}
