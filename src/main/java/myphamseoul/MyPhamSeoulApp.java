package myphamseoul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyPhamSeoulApp {
	public static void main(String[] args) {
		SpringApplication.run(MyPhamSeoulApp.class, args);
	}

}
